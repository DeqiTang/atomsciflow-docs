.. Atomsciflow Docs documentation master file, created by
   sphinx-quickstart on Mon Jan 10 13:09:06 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Atomsciflow Docs's documentation!
============================================
The AtomSciFlow project is a workflow manager for scientific research involving
atoms.

The Python interface
--------------------
Using of atomsciflow in python is like::

>>> from atomsciflow.gamessus import GamessUS 
>>> task = GamessUs()
>>> task.get_xyz("CH4.xyz")
>>> task.run("job-running")

.. toctree::
   :maxdepth: 2
   :caption: Contents:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
