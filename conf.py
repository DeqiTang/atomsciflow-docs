# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Atomsciflow Docs'
copyright = '2022, Deqi Tang'
author = 'Deqi Tang'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'recommonmark',
    'sphinx.ext.mathjax',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# muster set master_doc, or readthedocs.org will not be able to build the content
master_doc = "index"


# PDF related setting
# Ref: 
# * https://www.sphinx-doc.org/en/master/latex.html
# * https://docs.readthedocs.io/en/stable/guides/pdf-non-ascii-languages.html
# * https://github.com/readthedocs/readthedocs.org/issues/6319
latex_engine = 'xelatex'
latex_use_xindy = False
# xindy is currently not supported by Read the Docs, 
# but they plan to support it in the near future.
latex_elements = {
    'preamble': r'''
\usepackage[UTF8]{ctex}
\usepackage[titles]{tocloft}
\cftsetpnumwidth {1.25cm}\cftsetrmarg{1.5cm}
\setlength{\cftchapnumwidth}{0.75cm}
\setlength{\cftsecindent}{\cftchapnumwidth}
\setlength{\cftsecnumwidth}{1.25cm}
''',
    'fontpkg': r'''
% default English fonts
\setmainfont{DejaVu Serif} %{FreeSerif} %{DejaVu Serif} %{Noto Serif CJK SC}
\setsansfont{DejaVu Sans} %{FreeSans} %{DejaVu Sans} %{Noto Sans CJK SC}
\setmonofont{DejaVu Sans Mono} %{FreeMono} %{DejaVu Sans Mono} % {Noto Sans Mono CJK SC}

% use lower level xeCJK to deal with Chinese fonts
\usepackage {xeCJK}
\setCJKmainfont{Noto Serif CJK SC} % Source Han Serif
\setCJKsansfont{Noto Sans CJK SC} % Source Han Sans
\setCJKmonofont{Noto Sans Mono CJK SC}
''',
    'papersize': 'a4paper',
    'fncychap': r'\usepackage[Bjornstrup]{fncychap}',
    'printindex': r'\footnotesize\raggedright\printindex',
    'figure_align': 'H',        
}
latex_show_urls = 'no' # 'footnote'
